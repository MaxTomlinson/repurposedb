$(function(){ // on dom ready

$('#cy').cytoscape({
  style: cytoscape.stylesheet()
    .selector('node')
      .css({
        'content': 'data(name)',
        'text-valign': 'center',
        'color': 'data(color)',
        'text-outline-width': 2,
        'text-outline-color': '#000'
      })
    .selector('edge')
      .css({
        'target-arrow-shape': 'triangle'
      })
    .selector(':selected')
      .css({
        'background-color': 'black',
        'line-color': 'black',
        'target-arrow-color': 'black',
        'source-arrow-color': 'black'
      })
    .selector('.faded')
      .css({
        'opacity': 0.25,
        'text-opacity': 0
      }),
  
      elements: {
    	    nodes: [
    	      { data: { id: 'Rx00037', name: 'bacitracin', color: 'red'  } },
    	      { data: { id: 'D1', name: 'pseudomembranous enterocolitis', color: '#66CCFF'   } },
    	      { data: { id: 'D2', name: 'pneumonia', color: '#66CCFF'   } } ,
    	      { data: { id: 'D3', name: 'empyema', color: '#66CCFF'   } } ,
    	      { data: { id: 'D4', name: 'superficial ocular infection', color: '#66CCFF'   } } 
    	    ],
    	    edges: [
    	      { data: { source: 'Rx00037', target: 'D1' } },
    	      { data: { source: 'Rx00037', target: 'D2' } },
    	      { data: { source: 'Rx00037', target: 'D3' } },
    	      { data: { source: 'Rx00037', target: 'D4' } }
    	      ]
      },
  
  layout: {
    name: 'grid',
    padding: 10
  },
  
  // on graph initial layout done (could be async depending on layout...)
  ready: function(){
    window.cy = this;
    
    // giddy up...
    
    cy.elements().unselectify();
    
    cy.on('tap', 'node', function(e){
      var node = e.cyTarget; 
      var neighborhood = node.neighborhood().add(node);
      
      cy.elements().addClass('faded');
      neighborhood.removeClass('faded');
    });
    
    cy.on('tap', function(e){
      if( e.cyTarget === cy ){
        cy.elements().removeClass('faded');
      }
    });
  }
});

}); // on dom ready