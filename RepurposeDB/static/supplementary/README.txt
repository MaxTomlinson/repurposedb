Systematic analyses of drugs, diseases and drug targets in RepurpodeDB
Supplementary Data files in Excel format 

RepurposeDB_BinChe.xlsx
RepurposeDB_CGEA.xlsx
RepurposeDB_ChemicalProperties.xlsx
RepurposeDB_CPDB.xlsx
RepurposeDB_DAVID.xlsx
RepurposeDB_DiseaseSimilarity.xlsx
RepurposeDB_Drug_Food_Target_Networks.xlsx
RepurposeDB_EHR_SGA.xlsx
RepurposeDB_Enrichr.xlsx
RepurposeDB_EvidenceTypes.xlsx
RepurposeDB_PanOntology_Mapping.xlsx
RepurposeDB_Pharmacoeconomics.xlsx
RepurposeDB_PubMed_Articles.xlsx
RepurposeDB_SFN_EFN_NA.xlsx

Compiled on Aug 16 2016 
Compiled by Khader Shameer, Benjamin Glicksberg, Marcus A Badgeley, Tim O’ Connor & Max S Tomlinson

Contact us at repurposedb@dudleylab.org | skhadar@gmail.com 

Dataset accompnying the following manuscript: 
Systematic analyses of 253 drugs and 1125 disease indications in RepurposeDB reveal chemical, biological and epidemiological factors influencing drug repositioning Khader Shameer1, Benjamin S. Glicksberg1, Rachel Hodos1, Marcus A. Badgeley1, Kipp W Johnson1, Ben Readhead1 , Max S. Tomlinson1, Timothy O’Connor1,2, Riccardo Miotto1, Brian A. Kidd1, Rong Chen1, Avi Ma’ayan3 and Joel T. Dudley1,2*  1 Department of Genetics and Genomic Sciences, Harris Center for Precision Wellness, Icahn Institute for Genomics and Multiscale Biology, Icahn School of Medicine at Mount Sinai, Mount Sinai Health System, New York, New York, USA. 2 Boston College, Morrissey College of Arts and Sciences, Chestnut Hill, MA3 Department of Pharmacology and Systems Therapeutics, Icahn School of Medicine at Mount Sinai, Systems Biology Center New York (SBCNY), Mount Sinai Health System, New York, New York, USA. 4 Department of Population Health Science and Policy, Icahn School of Medicine at Mount Sinai, Mount Sinai Health System, New York, New York, USA.*Corresponding author: joel.dudley@mssm.edu 