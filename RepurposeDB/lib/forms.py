from flask.ext.wtf import Form, RecaptchaField    
from wtforms import StringField, TextField, RadioField, FileField, validators, SelectMultipleField, BooleanField, SelectField
from wtforms.fields.html5 import EmailField  
from wtforms.widgets import TextArea



# search form
class SearchForm(Form):
    search_text = StringField ('search_string')

class ContactForm(Form):
    topic = SelectField('Topic',
                        choices=[('question about the site', "I have a question about the site"),
                                 ('bug report', "I would like to report a bug"),
                                 ('feature suggestion', "I have a suggestion for a feature"),
                                 ('comment', "Other")])
    email = EmailField('Your Email', [validators.Email()])
    issue = StringField('Your Issue', [validators.Length(min=3, max=2000)], widget=TextArea())
    recaptcha = RecaptchaField()

class RxPredForm(Form):
    smile = StringField('smile', [validators.Length(min=3, max=1000), validators.Optional()])
    sdfFile = FileField('sdfFile', [validators.Optional()])
    protein = StringField('protein', [validators.Length(min=3, max=33000), validators.Optional()], widget=TextArea())
    proteinFile = FileField('proteinFile', [validators.Optional()])

class NonValidatingSelectMultipleField(SelectMultipleField):
    def pre_validate(self, form):
        pass

class SubmissionForm (Form):
    compoundName = TextField('Compound Name', [validators.Length(min=3, max=25)])
    compoundIdentifiers = TextField('Compound Identifiers', 
                                    [validators.Length(min=2, max=25)])
    approval = RadioField('FDA Approval', 
                          choices=[('yes', 'This indication is FDA-Approved<br>'), 
                                   ('no', 'This indication is <b>NOT</b> FDA-Approved')])
    primaryIndicationText = TextField('Primary Indication', [validators.Length(min=3, max=25), validators.Optional()])
    primaryIndicationSelect = NonValidatingSelectMultipleField('Primary Indications', choices=[])
    secondaryIndicationText = TextField('Secondary Indication', [validators.Length(min=3, max=25), validators.Optional()])
    secondaryIndicationSelect = NonValidatingSelectMultipleField('Secondary Indications', choices=[])
    experimentalEvidence = BooleanField('Experimental Evidence')
    computationallyRepurposedEvidence = BooleanField('Computationally Repurposed Evidence')
    clinicalValidation = BooleanField('Clinical Trial Validation')
    ehrBasedValidation = BooleanField('EHR Based Validation')
    references = StringField('Text', widget=TextArea())
    yourName = TextField('Your Name', [validators.Length(min=2, max=25)])
    yourEmail = EmailField('Your Email', [validators.DataRequired(), validators.Email()])
    recaptcha = RecaptchaField()
    
