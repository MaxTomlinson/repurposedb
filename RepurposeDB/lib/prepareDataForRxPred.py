from tempfile import NamedTemporaryFile
import subprocess, os, sys
#import openbabel
#import pybel
import arff #liac-arff
from file import makeIDSet, read_csv, write_csv, makeIDLookup
#from parseData import loadDrugBankXML, getApproved
from math import log10, ceil
from operator import itemgetter

def smilesToSDF(sdfFile, smileFile = None, opt=False, justOne = True):
    inputType = 'smi'
    if not smileFile:
        smileFile = sdfFile
        inputType = 'sdf'
    if justOne:
        bashCommand = "babel -h --gen3D -l 1 -i"+inputType+" " + smileFile + " -osdf " + sdfFile
    else:
        bashCommand = "babel -h --gen3D -i"+inputType+" " + smileFile + " -osdf " + sdfFile
    
    process = subprocess.Popen(bashCommand.split(), stdout=subprocess.PIPE)
    #output = process.communicate()[0]
    #print output
    process.wait()
    
    if opt:
        bashCommand = "obminimize -n 500 -ff MMFF94 " + sdfFile
        process = subprocess.Popen(bashCommand.split(), stdout=subprocess.PIPE)
        #output = process.communicate()[0]
        #print output
        process.wait()

def blueDescSDF(filepath, basepath, memory = '512M'):
    bashCommand = "java -jar -Xmx"+memory+" "+basepath+"lib/ODDescriptors.jar -f "+filepath+" -l DrugBankID"
    process = subprocess.Popen(bashCommand.split(), stdout=subprocess.PIPE)
    #output = process.communicate()[0]
    #print output
    process.wait()

def arffToCSV(arffPath, csvPath):
    arffFile = open(arffPath, 'rb')
    arffData = arff.ArffDecoder().decode(arffFile)
    arffFile.close()
    csvData = []
    header = []
    for col in arffData['attributes']:
        header.append(col[0])
    csvData.append(header)
    csvData.extend(arffData['data'])
        
    return write_csv(csvPath, csvData)


def annotateSDF(sdfFilepath, outfile, basepath, smilefile = None, memory = '512M', arffLoaded = False):
    if smilefile: smilesToSDF(sdfFilepath, smileFile = smilefile, opt=True, justOne = True)
    arffFilepath = sdfFilepath.replace(".sdf", '.oddescriptors.arff')
    if not arffLoaded: blueDescSDF(sdfFilepath, basepath, memory= memory)
    arffToCSV(arffFilepath, outfile)



#Before having to switch to command line
# def smileToMolecule(SMILE, desc = False, memory = '512M', opt=False, twoDimensional = False, ODDdir=''):
#     mol = pybel.readstring("smi", SMILE)
#     if twoDimensional: 
#         mol.removeh() #doesn't work if SMILE has explicit hydrogens
#         mol.draw(show=False, update=True) 
#     else:
#         mol.addh()
#         mol.make3D()
#     if opt: 
#         mol.localopt()
#     if desc: 
#         mol = annotateMolecule(mol, memory = memory, ODDdir = ODDdir)
#     return mol
#     #mol.write(format='sdf', filename=filename, overwrite = overwrite)
# 
# def smilesToSDF(SMILEs, filename, overwrite=False, opt=False, desc = False):
#     out = pybel.Outputfile('sdf', filename, overwrite=overwrite)
#     if isinstance(SMILEs, list):
#         for SMILE in SMILEs:
#             mol = smileToMolecule(SMILE, opt=opt, desc = desc)
#             mol.data['DrugBankID']=mol.title.replace("DB", "")
#             if testCoords(mol): 
#                 out.write(mol)
#                 print "Processed " + mol.title
#             else:
#                 print "Did not include " + mol.title
#     else:
#         mol = smileToMolecule(SMILEs, opt=opt, desc = desc)
#         mol.data['DrugBankID']=mol.title.replace("DB", "")
#         out.write(mol)
#         print "Processed " + mol.title
#     out.close()
# 
# def testCoords(mol):
#     for atom in mol.atoms:
#         if 'nan' in str(atom.coords):
#             return False
#     return True
# 
# def blueDescSDF(filepath, memory = '512M', ODDdir = ''):
#      bashCommand = "java -jar -Xmx"+memory+" " +ODDdir + "ODDescriptors.jar -f "+filepath+" -l DrugBankID"
#      process = subprocess.Popen(bashCommand.split(), stdout=subprocess.PIPE)
#      output = process.communicate()[0]
#      print output
#      process.wait()
# 
# def arffToCSV(arffPath, csvPath, extraColumns = None, extraRows = False):
#     arffFile = open(arffPath, 'rb')
#     arffData = arff.ArffDecoder().decode(arffFile)
#     arffFile.close()
#     csvData = []
#     if not extraColumns:
#         header = []
#         for col in arffData['attributes']:
#             header.append(col[0])
#         #header.remove('ignore_this')
#         csvData.append(header)
#         csvData.extend(arffData['data'])
#     else:
#         indexOfID = len(arffData['attributes'])-1
#         header = extraColumns['header']
#         arffData['attributes'].pop() #removes DrugBankID from end of list
#         for col in arffData['attributes']:
#             header.append(col[0])
#         csvData.append(header)
#         if extraRows: hasNotBeenWritten = extraColumns.keys()
#         for row in arffData['data']:
#             DBID = numToDBID(row[indexOfID])
#             locallist = [DBID]
#             locallist.extend(extraColumns[DBID])
#             row.pop() #removes DrugBankID from end of list
#             locallist.extend(row)
#             csvData.append(locallist)
#             if extraRows: hasNotBeenWritten.remove(DBID)
#         if extraRows:
#             hasNotBeenWritten.remove('header')
#             for DBID in hasNotBeenWritten:
#                 locallist = [DBID]
#                 locallist.extend(extraColumns[DBID])
#                 csvData.append(locallist)
#             sorted(csvData, key = itemgetter(0))
#         
#     return write_csv(csvPath, csvData)
# 
# def numToDBID(id):
#     digits = ceil(log10(id+.1))
#     zeroes = int(5-digits)*'0'
#     return 'DB'+zeroes+str(int(id))
# 
# def annotateMolecule(mol, memory = '512M', ODDdir=''):
#     f = NamedTemporaryFile(delete = 'false')
#     basepath = f.name
#     f.close()
#     sdfFilepath = basepath+'.sdf'
#     arffFilepath = basepath+'.oddescriptors.arff'
#     oddFilepath = basepath+'.lp.oddescriptors.att'
#     
#     mol.write(format = 'sdf', filename=sdfFilepath)
#     blueDescSDF(sdfFilepath, memory= memory, ODDdir=ODDdir)
#     
#     arffFile = open(arffFilepath, 'rb')
#     arffData = arff.ArffDecoder().decode(arffFile)
#     arffFile.close()
#     
#     for index, col in enumerate(arffData['attributes']):
#         mol.data[str(col[0])] = arffData['data'][0][index]
#     os.remove(sdfFilepath)
#     os.remove(arffFilepath)
#     os.remove(oddFilepath)
#     return mol
# 
# def annotateSMILEs(SMILEs, outfile, sdfFilepath = None, extraColumns = [], extraRows = False, memory = '512M', overwrite = False, opt = False, desc = False, sdfLoaded = False, arffLoaded = False, ODDdir=''):
#     f = NamedTemporaryFile(delete = 'false')
#     basepath = f.name
#     f.close()
#     
#     deleteSDF = False
#     if not sdfFilepath:
#          sdfFilepath = basepath+'.sdf'
#          deleteSDF = True
#     arffFilepath = sdfFilepath.replace(".sdf", '.oddescriptors.arff')
#     oddFilepath = sdfFilepath.replace(".sdf", '.lp.oddescriptors.att')
#     
#     if not sdfLoaded: smilesToSDF(SMILEs, sdfFilepath, overwrite=overwrite, opt=opt, desc = desc)
#     annotateSDF(sdfFilepath, outfile, extraColumns = extraColumns, extraRows = extraRows, memory = memory, arffLoaded = arffLoaded, ODDdir=ODDdir)
#     if deleteSDF: os.remove(sdfFilepath)
#     os.remove(arffFilepath)
#     os.remove(oddFilepath)
# 
# def annotateSDF(sdfFilepath, outfile, extraColumns = [], extraRows = False, memory = '512M', arffLoaded = False, ODDdir=''):
#     arffFilepath = sdfFilepath.replace(".sdf", '.oddescriptors.arff')
#     if not arffLoaded: blueDescSDF(sdfFilepath, memory= memory, ODDdir=ODDdir)
#     arffToCSV(arffFilepath, outfile, extraColumns = extraColumns, extraRows = extraRows)
