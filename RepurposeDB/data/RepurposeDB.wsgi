import site
site.addsitedir('/opt/RepurposeDB-Python-Production/env/lib/python2.7/site-packages')
import sys
sys.path.insert(0, '/opt/RepurposeDB-Python-Production/app')
sys.stdout = sys.stderr
from RepurposeDB import app as application